export class DomModule{
    
    constructor(){}
    
    static addPhotosToDom(photo, index){
        let className = "img" + index;
        let div = document.getElementsByClassName(className)[0];
        div.style.backgroundImage = "url(" + photo.url + ")";
        div.classList += " showImg";
    }

}