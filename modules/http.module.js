import { ApplicationModule } from "./application.module.js";
import { DomModule } from "./dom.module.js";

export class HttpModule{

    _url = window.location.origin;

    constructor(){}

    async _get(photoId){
        return await fetch(this._url + "/assets/img/"+ photoId +".jpg");
    }

    async getAllPhotos(){
        let photos = [];
        for(let i = 1; i < ApplicationModule.totalPhotos; i++){
            photos.push(i);
        }

        photos.reduce( async (previousPromise, nextId) => {
            await previousPromise;
            let photo = await this._get(nextId);
            DomModule.addPhotosToDom(photo,nextId);
            return photo;
        }, Promise.resolve());
    }

}