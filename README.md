# CSS3 Collage Gallery its a ...

... project made to test the powerfull CSS3, HTML5 & ECMAScript new features. The only propusal behind this project is to learn and practice this new features, like *filters*, *grid*, *arrow functions*... This is why I recommend to [open the demo](https://emidev98.gitlab.io/css3-collage-gallery/) using the last version of the web browser.

## Content from

Images from [Unsplash](https://unsplash.com/) & [Pexels](https://www.pexels.com/).

Song name [GRAPES - I dunno](https://www.youtube.com/watch?v=gaarci0mTDs)