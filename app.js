import { HttpModule } from "./modules/http.module.js";
import { DomModule } from "./modules/dom.module.js";


let init = async ()=>{
    let httpModule = new HttpModule();
    httpModule.getAllPhotos();
};

init();